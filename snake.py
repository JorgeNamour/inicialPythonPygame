import pygame
import sys
import random

#NOTA: este codigo es el que desarrollamos en clases, queda a criterio del alumno la escala a utilizar.

tama = [330,330]
ventana = pygame.display.set_mode(tama) #Surface
pygame.display.set_caption("Snake")

###
#config
colores = {"fondo": (0,0,0), "cuerpo": (0,0,200), "fruta": (200,0,0)}
posicionInicialCabeza = [tama[0]/2, tama[1]/2]
dimensionCuerpo = (30,30)
dimensionFruta = (10,10)
desplazamiento = dimensionCuerpo[0]
banderaDesplazamiento = 0
listaCuerpo = []


###
#clases
class Cuerpo:
    def __init__(self):
        self.rect = pygame.Rect(posicionInicialCabeza, dimensionCuerpo)

    def set_posicion(self, rect):
        self.rect = rect

    def get_posicion(self):
        return self.rect

    def pinta_de_negro(self):
        ventana.fill(colores["fondo"], self.rect)

    def dibujar(self):
        ventana.fill(colores["cuerpo"], self.rect)

    def mover(self, bandera):
        if bandera == 1:
            self.rect.top -= desplazamiento
        elif bandera == 2:
            self.rect.left += desplazamiento
        elif bandera == 3:
            self.rect.top += desplazamiento
        elif bandera == 4:
            self.rect.left -= desplazamiento

    def come(self, fruta):
        return self.rect.colliderect(fruta.get_posicion())

    def choca(self, cuerpo):
        if len(cuerpo) > 0:
            for parte in cuerpo:
                if self.rect.colliderect(parte.get_posicion()):
                    return True

        if self.rect.left < 0 or self.rect.right > tama[0] or self.rect.top < 0 or self.rect.bottom > tama[1]:
            return True

class Fruta:
    def __init__(self):
        self.rect = pygame.Rect((random.randint(0,tama[0]-dimensionFruta[0]),random.randint(0,tama[1]-dimensionFruta[1])),dimensionFruta)

    def get_posicion(self):
        return self.rect

    def redefinir_posicion(self):
        self.rect.left = random.randint(0,tama[0]-dimensionFruta[0])
        self.rect.top = random.randint(0,tama[1]-dimensionFruta[1])

    def dibujar(self):
        ventana.fill(colores["fruta"], self.rect)

def reset():
    global banderaDesplazamiento
    banderaDesplazamiento = 0
    ventana.fill(colores["fondo"])
    rectAux = pygame.Rect(posicionInicialCabeza, dimensionCuerpo)
    cabeza.set_posicion(rectAux)
    cabeza.dibujar()
    fruta.redefinir_posicion()
    global listaCuerpo
    listaCuerpo = []
    

cabeza = Cuerpo()
fruta = Fruta()

###
#game loop
while True:
    pygame.time.delay(200)
    posicionActualCabeza = pygame.Rect(cabeza.get_posicion())

    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            sys.exit()
            pygame.quit()

        if evento.type == pygame.KEYDOWN:
            if evento.key == pygame.K_UP:
                banderaDesplazamiento = 1
            elif evento.key == pygame.K_RIGHT:
                banderaDesplazamiento = 2
            elif evento.key == pygame.K_DOWN:
                banderaDesplazamiento = 3
            elif evento.key == pygame.K_LEFT:
                banderaDesplazamiento = 4

    cabeza.pinta_de_negro()
    cabeza.mover(banderaDesplazamiento)
    cabeza.dibujar()

    if cabeza.come(fruta):
        fruta.redefinir_posicion()
        listaCuerpo.append(Cuerpo())

    if len(listaCuerpo) > 0:
        listaCuerpo[0].pinta_de_negro()
        listaCuerpo[0].set_posicion(posicionActualCabeza)
        listaCuerpo[0].dibujar()
        listaCuerpo.append(listaCuerpo[0])
        listaCuerpo.pop(0)

    if cabeza.choca(listaCuerpo):
        reset()

    fruta.dibujar()

    pygame.display.update()    
