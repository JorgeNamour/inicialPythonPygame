import pygame
import sys
import random

pygame.font.init()
###
#configuracion
tama = (330,330)
ventana = pygame.display.set_mode(tama) 
pygame.display.set_caption("Mi juego")
colores = {"fondo": (0,0,0), "comida": (200,0,0), "cuerpo": (0,0,200)}
cuerpo = []
posicionInicial = [0,0]#[tama[0]/2, tama[1]/2]
dimensionCuerpo = (30,30)
dimensionComida = (10,10)
desplazamiento = dimensionCuerpo[0]
banderaDesplazamiento = 0

miFuente = pygame.font.Font(None, 30)

###
#clases
class Cuerpo():
    def __init__(self, num):
        self.num = num
        self.rect = pygame.Rect(posicionInicial, dimensionCuerpo) #Rect

    def mover(self, bandera):
        if bandera == 1:
            self.rect.top -= desplazamiento
        elif bandera == 2:
            self.rect.right += desplazamiento
        elif bandera == 3:
            self.rect.top += desplazamiento
        elif bandera == 4:
            self.rect.left -= desplazamiento

    def dibujar(self):
        ventana.fill(colores["cuerpo"], self.rect)

    def pintar(self):
        ventana.fill(colores["fondo"], self.rect)

    def come(self, comida):
        return self.rect.colliderect(comida.get_posicion())

    def get_posicion(self):
        return self.rect

    def set_posicion(self, rect):
        self.rect = rect

    def choque(self, cuerpo):
        if self.rect.right > tama[0] or self.rect.left < 0 or self.rect.top < 0 or self.rect.bottom > tama[1]:
            return True
        if len(cuerpo) > 0:
            for parte in cuerpo:
                if parte.get_posicion().colliderect(self.rect):
                    return True

    def get_num(self):
        return self.num

class Comida():
    def __init__(self):
        self.rect = pygame.Rect((random.randint(0,tama[0]-dimensionComida[0]),random.randint(0,tama[1]-dimensionComida[1])),dimensionComida)

    def dibujar(self):
        ventana.fill(colores["comida"], self.rect)

    def redefinir_posicion(self):
        self.rect.left = random.randint(1,11)*15
        self.rect.top = random.randint(1,11)*15n
        # self.rect.left = random.randint(0,tama[0]-dimensionComida[0])
        # self.rect.top = random.randint(0,tama[1]-dimensionComida[1])

    def get_posicion(self):
        return self.rect

def reiniciar():
    global cuerpo
    cuerpo = []
    global banderaDesplazamiento
    banderaDesplazamiento = 0
    ventana.fill(colores["fondo"])
    rectAux = pygame.Rect(posicionInicial, dimensionCuerpo)
    cabeza.set_posicion(rectAux)
    cabeza.dibujar()
    comida.redefinir_posicion()
    comida.dibujar()

###
#game loop
cabeza = Cuerpo(-1)
cabeza.dibujar()

comida = Comida()

while True:
    pygame.time.delay(300)
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

        if evento.type == pygame.KEYDOWN:
            if evento.key == pygame.K_UP:
                banderaDesplazamiento = 1
            elif evento.key == pygame.K_RIGHT:
                banderaDesplazamiento = 2
            elif evento.key == pygame.K_DOWN:
                banderaDesplazamiento = 3
            elif evento.key == pygame.K_LEFT:
                banderaDesplazamiento = 4

    if banderaDesplazamiento != 0:
        posicionCabecaActual = pygame.Rect(cabeza.get_posicion())
        cabeza.pintar()
        cabeza.mover(banderaDesplazamiento)
        cabeza.dibujar()

        if cabeza.choque(cuerpo):
            reiniciar()

        if cabeza.come(comida):
            comida.redefinir_posicion()
            cuerpo.append(Cuerpo(len(cuerpo)))

        if len(cuerpo) > 0:
            cuerpo[0].pintar()
            cuerpo[0].set_posicion(posicionCabecaActual)
            cuerpo[0].dibujar()
            miTexto = miFuente.render(str(cuerpo[0].get_num()), 0, (200,200,200))
            ventana.blit(miTexto, (cuerpo[0].get_posicion().left,cuerpo[0].get_posicion().top) )
            cuerpo.append(cuerpo[0])
            cuerpo.pop(0)

    comida.dibujar()
    pygame.display.update()