import pygame
import sys

tama = 400,400	
colorFondo = (0,0,0)

ventana = pygame.display.set_mode(tama)
pygame.display.set_caption("Ventana")

obj1 = pygame.image.load("marciano.png")
rectObj = obj1.get_rect()
rectObj.left = 50
rectObj.top = 50

desplazamiento = 5

while True:
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    
    if rectObj.left < 0 or rectObj.right > tama[1]:
        desplazamiento *= -1

    rectObj = rectObj.move((desplazamiento, 0))

    ventana.fill(colorFondo)
    ventana.blit(obj1, rectObj)
    pygame.display.update()