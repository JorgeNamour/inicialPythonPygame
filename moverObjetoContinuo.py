import pygame
import sys

tama = [500,500]
colorFondo = (0,0,0)

sup = pygame.display.set_mode(tama)

img = pygame.image.load("marciano.png")

rect = img.get_rect()

teclaIzquierda = False
teclaDerecha = False
teclaArriba = False
teclaAbajo = False
despX = 1
despY = 1

rect.left = 100
rect.top = 100

while True:
    pygame.time.delay(20)
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            sys.exit()        
        elif event.type == pygame.KEYDOWN:         
            if event.key == pygame.K_LEFT:        
                teclaIzquierda = True
            elif event.key == pygame.K_RIGHT:     
                teclaDerecha = True
            elif event.key == pygame.K_UP:        
                teclaArriba = True
            elif event.key == pygame.K_DOWN:     
                teclaAbajo = True
        elif event.type == pygame.KEYUP:         
            if event.key == pygame.K_LEFT:       
                teclaIzquierda = False
            elif event.key == pygame.K_RIGHT:    
                teclaDerecha = False
            elif event.key == pygame.K_UP:       
                teclaArriba = False
            elif event.key == pygame.K_DOWN:     
                teclaAbajo = False

    if teclaIzquierda:
        rect.left -= despX
    if teclaDerecha:
        rect.left += despX
    if teclaArriba:
        rect.top -= despY
    if teclaAbajo:
        rect.top += despY

    sup.fill(colorFondo)
    sup.blit(img, rect)
    pygame.display.update()
