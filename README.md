Seminario "Introducción a Python + Pygame" - Laboratorio de Computación Científica (LABCC) - Departamento Ciencias de la Computación - FACET - UNT.

####################################

Herramientas utilizadas:
* Python v2.7.6
* Pygame v1.9.2

####################################

Se añade:
* Desplazamiento continuo de un objeto sobre superficie.
* Archivo "pongSolucion.py", solución de "pongEjercicio.py"

####################################