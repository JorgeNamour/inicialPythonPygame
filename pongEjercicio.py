import sys
import pygame

tama = ancho, alto = (600,600)
#[Instancie una ventana hacinedo uso de la tupla "tama"]

dimensionJugador = 30,80
dimensionPelota = 30,30
colores = {"jugador": (0,0,200), "pelota": (255,255,255), "fondo": (0,0,0)}
posicionPelota = [ancho/2, alto/2]
posicionJugador1 = [0,alto/2]
posicionJugador2 = [ancho - dimensionPelota[0],alto/2]
teclasJugador1 = (pygame.K_s, pygame.K_w)
#[Defina las teclas del jugador 2 utilizando las variables locales de Pygame]

movimientoPelota = [2,3]
movimientoJugador = tama[1]/30

class Paleta():
    def __init__(self, posicionJugador, teclaJugador):
        self.teclasJugador = teclaJugador
        #[Armar el rectangulo contenedor de la pelota, utilice los parametros de entrada]

    def dibujar(self):
        ventana.fill(colores["jugador"], self.rect)

    def mover(self, tecla):
        if tecla == self.teclasJugador[0] and self.rect.bottom < ventana.get_height():
            self.rect.top += movimientoJugador
        #[Defina el movimiento hacia arriba de los jugadores]

    def posicion(self):
        return self.rect

class Pelota():
    def __init__(self):
        self.rect = pygame.Rect(posicionPelota, dimensionPelota)

    def dibujar(self):
        [Dibujar la pelota]
        ventana.fill(colores["pelota"], self.rect)

    def mover(self):
        if self.rect.left <= 0 or self.rect.right >= ancho:
            movimientoPelota[0] *= -1
        if self.rect.top <= 0 or self.rect.bottom >= alto:
            movimientoPelota[1] *= -1
        self.rect = self.rect.move(movimientoPelota)

    def colision(self, jugador1, jugador2):
        #[Defina la accion a realizar cuando la pelota colisiona con alguna de las paletas]

jugador1 = Paleta(posicionJugador1, teclasJugador1)
jugador2 = Paleta(posicionJugador2, teclasJugador2)
pelota = Pelota()
    
while True:
    pygame.time.delay(20)
    ventana.fill(colores["fondo"])
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if evento.type == pygame.KEYDOWN:
            if evento.key in teclasJugador1:
                jugador1.mover(evento.key)
            #[Determine los eventos para los cuales se mueve el jugador 2]

    jugador1.dibujar()
    jugador2.dibujar()
    pelota.mover()
    pelota.colision(jugador1, jugador2)
    pelota.dibujar()
    pygame.display.update()  